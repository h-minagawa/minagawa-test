<?php
/*
Template Name: media
*/
?>
<?php /* WordPress CMS Theme media */ get_header(); ?>
<main>
<div class="inner">
<!-- パンくずリスト -->
<?php if( !is_home() && !is_front_page() ) :?>
<ol class="breadcrumb">
  <li >
    <a href="/" >
      <span>ホーム</span>
    </a>
  </li>
  <li>
     <span><?php the_title(); ?></span>
  </li>
</ol>
<?php endif; ?>
<!-- パンくずリスト -->
<?php
if(have_posts()): while(have_posts()): the_post();?>
<h2><?php the_title(); ?></h2>

<?php the_content(); ?>

<?php endwhile; ?>
<?php else: ?>
<div class="notfound"><p>ページがありません</p></div>
<?php endif; ?>
</div>
</main>
<?php get_footer(); ?>