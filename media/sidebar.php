<!-----sidebar----->
	  <div id="side_menu">
      <div class="ranking">
         <h2>
             ランキング<br><span>RANKING</span>
         </h2>
          <section class="side_menu_inner">         
            <?php
            $ranking_post = new WP_Query(
                array(
                    'post_type'      => 'post',// ページタイプを指定
                    'posts_per_page' => 5,// 取得する投稿数 「-1」指定で全件表示
                    'orderby' => 'date', // 日付でソート
                    'order' => 'DESC', // DESCで最新から表示、ASCで最古から表示
                    'category_name' => 'uncategorized,app,event,people,shop,life,design,product,photo,movie,local,interior,trip,book,eat' // 表示したいカテゴリーのスラッグを指定
                )
            );
            ?>
            <?php if ( $ranking_post->have_posts() ) : ?>
            <?php while ( $ranking_post->have_posts() ) : ?>
            <?php $ranking_post->the_post(); ?>
            <article>
                <a class="ranking_link" href="<?php the_permalink(); ?>">
                <!--画像を追加-->
      			    <?php if( has_post_thumbnail() ): ?>
       			    <?php the_post_thumbnail(); ?>
      			    <?php endif; ?>
                <!--タイトル-->
                <h3><?php the_title(); ?></h3>
                </a>
            </article>
        <?php endwhile; ?>
        <?php endif; ?>
        <?php wp_reset_postdata(); ?>
          </section> 
      </div>
      <div class="category">
        <h2>
            カテゴリー<br>
            <span>CATEGORY</span>
        </h2>
        <section class="side_menu_inner">
           <article>
            <a href="<?php echo home_url().'/'; ?>magazine/design">デザイン</a>
           </article>
           <article>
            <a href="<?php echo home_url().'/'; ?>magazine/product">モノ</a>
           </article>
           <article>
            <a href="<?php echo home_url().'/'; ?>magazine/interior">建築・インテリア</a>
           </article>
           <article>
            <a href="<?php echo home_url().'/'; ?>magazine/photo">写真</a>
           </article>
           <article>
            <a href="<?php echo home_url().'/'; ?>magazine/movie">動画</a>
           </article>
           <article>
            <a href="<?php echo home_url().'/'; ?>magazine/app">アプリ</a>
           </article>
           <article>
            <a href="<?php echo home_url().'/'; ?>magazine/book">本</a>
           </article>
           <article>
            <a href="<?php echo home_url().'/'; ?>magazine/life">くらし</a>
           </article>
           <article>
            <a href="<?php echo home_url().'/'; ?>magazine/trip">旅</a>
           </article>
           <article>
            <a href="<?php echo home_url().'/'; ?>magazine/shop">お店・場</a>
           </article>
           <article>
            <a href="<?php echo home_url().'/'; ?>magazine/eat">食</a>
           </article>
           <article>
            <a href="<?php echo home_url().'/'; ?>magazine/event">イベント・展示</a>
           </article>
           <article>
            <a href="<?php echo home_url().'/'; ?>magazine/people">インタビュー</a>
           </article>
           <article>
            <a href="<?php echo home_url().'/'; ?>magazine/local">地域</a>
           </article>
          </section> 
      </div>
      <div class="banner">
          <section class="side_menu_inner">
           <article>
            <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/haconiwacreators_bunner.jpg" alt="広告1"></a>
           </article>
           <article>
            <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/susono.jpg" alt="広告2"></a>
           </article>
           <article>
            <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/haconiwa_design_store.jpg" alt="広告3"></a>
           </article>
          </section> 
      </div>
    </div>
<!-----//sidebar----->