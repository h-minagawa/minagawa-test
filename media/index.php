<?php /* WordPress CMS Theme media */ get_header(); ?>
<main>
    <?php if( have_posts() ) : ?>
    <?php while( have_posts() ) : the_post(); ?>
    <?php the_content(); ?>
    <?php endwhile; ?>
    <?php else: ?>
    <div class="notfound"><p>ページがありません</p></div>
    <?php endif; ?>
</main>
<?php get_footer(); ?>