<?php /* WordPress CMS Theme media */ get_header(); ?>
<main>
<!-----archive_content----->
<div class="archive_content">
	<div class="archive_wrap">
	<ol class="breadcrumb">
  <li>
    <a href="/" >
      <span>ホーム</span>
    </a>
  </li>
  <li>すべての記事</li>
</ol>
		<h2>すべての記事</h2>
		<section class="archive_inner">
			<?php
        $paged = get_query_var('page');
        $new_post = new WP_Query(
        array(
            'post_type'      => 'post',// ページタイプを指定
            'posts_per_page' => 9,// 取得する投稿数 「-1」指定で全件表示
            'orderby' => 'date', // 日付でソート
            'order' => 'DESC', // DESCで最新から表示、ASCで最古から表示
            'category_name' => 'uncategorized,app,event,people,shop,life,design,product,photo,movie,local,interior,trip,book,eat' // 表示したいカテゴリーのスラッグを指定
            )
         );
    ?>
       <?php if ( $new_post->have_posts() ) : ?>
          <?php while ( $new_post->have_posts() ) : ?>
            <?php $new_post->the_post(); ?>
       <article>
          <a href="<?php the_permalink(); ?>">
            <!--画像を追加-->
      			<?php if( has_post_thumbnail() ): ?>
       			<?php the_post_thumbnail(); ?>
      			<?php endif; ?>
            <!--カテゴリー-->
            <p class="magazine_category">
            <?php $str = '';
                foreach((get_the_category()) as $cat){
                    $str .= $cat->cat_name . ', ';
                }
                echo rtrim($str, ", "); ?></p>
            <!--タイトル-->
            <h3><?php the_title(); ?></h3>
            <!--投稿日を表示-->
            <p class="magazine_data"><?php echo get_the_date(); ?></p>
          </a>
        </article>
         <?php endwhile; ?>
        <?php endif; ?>
        <?php wp_reset_postdata(); ?>
        </section>
        <?php
					//Pagenation 
					if (function_exists("pagination")) {
					pagination($additional_loop->max_num_pages);
					}
					?>
</div>
  </div>
</main>
<!-----//archive_content----->
<?php get_footer(); ?>