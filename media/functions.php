<?php /* WordPress CMS Theme  */
// サムネイルの設定
add_theme_support( 'post-thumbnails' );

// タイトルタグ
add_filter( 'wp_title', 'my_filter_title', 10 );
function my_filter_title( $title ) {
	return $title. get_bloginfo( 'name' );
}

// headタグを整理
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10);// rel="next"、rel="prev"

// ショートコードの登録
function shortcode_url() {
    return get_bloginfo('url');
}
add_shortcode('url', 'shortcode_url');

function shortcode_templateurl() {
    return get_bloginfo('template_url');
}
add_shortcode('template_url', 'shortcode_templateurl');

// ショートコードをウィジェットで有効に
add_filter('widget_text', 'do_shortcode' );


//コラムページ　関連記事のサムネイル
add_theme_support('post-thumbnails');
set_post_thumbnail_size('full');

//投稿アーカイブページの作成
 function post_has_archive( $args, $post_type ) {

 	if ( 'post' == $post_type ) {
 		$args['rewrite'] = true;
 		$args['has_archive'] = 'magazine'; //任意のスラッグ名
 	}
 	return $args;

 }
 add_filter( 'register_post_type_args', 'post_has_archive', 10, 2 );

/**
 * 投稿のアーカイブページを設定
 */
add_filter('register_post_type_args', function($args, $post_type) {
    if ('post' == $post_type) {
        global $wp_rewrite;
        $archive_slug = 'magazine';
        $args['rewrite'] = true;
        $args['label'] = '記事投稿';
        $args['has_archive'] = $archive_slug;
        $archive_slug = $wp_rewrite->root.$archive_slug;
        $feeds = '(' . trim( implode('|', $wp_rewrite->feeds) ) . ')';
        add_rewrite_rule("{$archive_slug}/?$", "index.php?post_type={$post_type}", 'top');
        add_rewrite_rule("{$archive_slug}/feed/{$feeds}/?$", "index.php?post_type={$post_type}".'&feed=$matches[1]', 'top');
        add_rewrite_rule("{$archive_slug}/{$feeds}/?$", "index.php?post_type={$post_type}".'&feed=$matches[1]', 'top');
        add_rewrite_rule("{$archive_slug}/{$wp_rewrite->pagination_base}/([0-9]{1,})/?$", "index.php?post_type={$post_type}".'&paged=$matches[1]', 'top');
    }
    return $args;
}, 10, 2);



//ページネーション
//Pagenation
function pagination($pages = '', $range = 2)
{
     $showitems = ($range * 2)+1;//表示するページ数（５ページを表示）

     global $paged;//現在のページ値
     if(empty($paged)) $paged = 1;//デフォルトのページ

     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;//全ページ数を取得
         if(!$pages)//全ページ数が空の場合は、１とする
         {
             $pages = 1;
         }
     }

     if(1 != $pages)//全ページが１でない場合はページネーションを表示する
     {
		 echo "<div class=\"pagination\">\n";
		 echo "<ul>\n";
		 //Prev：現在のページ値が１より大きい場合は表示
         if($paged > 1) echo "<li class=\"prev\"><a href='".get_pagenum_link($paged - 1)."'>Prev</a></li>\n";

         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                //三項演算子での条件分岐
                echo ($paged == $i)? "<li class=\"active\">".$i."</li>\n":"<li><a href='".get_pagenum_link($i)."'>".$i."</a></li>\n";
             }
         }
		//Next：総ページ数より現在のページ値が小さい場合は表示
		if ($paged < $pages) echo "<li class=\"next\"><a href=\"".get_pagenum_link($paged + 1)."\">Next</a></li>\n";
		echo "</ul>\n";
		echo "</div>\n";
     }
}

// URLからcetegoryを消す
add_filter('user_trailingslashit', 'rem_cat_func');
function rem_cat_func($link) {
    return str_replace("/category/", "/", $link);
}
add_action('init', 'rem_cat_flush_rules');
function rem_cat_flush_rules() {
    global $wp_rewrite;
    $wp_rewrite->flush_rules();
}
add_filter('generate_rewrite_rules', 'rem_cat_rewrite');
function rem_cat_rewrite($wp_rewrite) {
    $new_rules = array('(.+)/page/(.+)/?' => 'index.php?category_name='.$wp_rewrite->preg_index(1).'&paged='.$wp_rewrite->preg_index(2));
    $wp_rewrite->rules = $new_rules + $wp_rewrite->rules;
}

?>