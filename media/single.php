<?php /* WordPress CMS Theme Shibuya Movie */ get_header(); ?>
<main>
<?php if( have_posts() ) : ?>
<?php while( have_posts() ) : the_post(); ?>
    <div class="single_post">
    <div class="single_post_inner">
    <ol class="breadcrumb">
  <li >
    <a href="/" >
      <span>ホーム</span>
    </a>
  </li>
  <li><a href="<?php echo home_url().'/'; ?>magazine">すべての記事</a></li>
  <li><?php 
  $category = get_the_category(); 
  if ( $category[0] ) {
    echo '<a href="' . get_category_link( $category[0]->term_id ) . '">' . $category[0]->cat_name . '</a>';
  }
?></li>
  <li>
      <span><?php the_title(); ?></span>
  </li>
</ol>
	<div class="content">
	<!-- パンくずリスト -->
<!-- パンくずリスト -->
	<h2><?php 
  $category = get_the_category(); 
  if ( $category[0] ) {
    echo '<a href="' . get_category_link( $category[0]->term_id ) . '">' . $category[0]->cat_name . '</a>';
  }
?></h2>
	<p><?php echo get_the_date(); ?></p>
		<h3><?php the_title(); ?></h3>
		<?php the_content(); ?>
    <div class="single_post_btn">
					<ul>
						<li class="prevpost">
						<?php $prev_poxt = get_previous_post();
							if (!empty( $prev_poxt  )):   ?> 
							<a href="<?php echo get_permalink( $prev_poxt->ID ); ?>
							"><?php echo get_the_title( $prev_poxt->ID ); ?></a>
							<?php endif;   ?>
						</li>
						<li class="nextpost">
						<?php $next_post = get_next_post();  
							if (!empty( $next_post )):  ?> 
							<a href="<?php echo get_permalink( $next_post->ID ); ?>
							"><?php echo get_the_title( $next_post->ID ); ?></a>
							<?php endif;   ?>
						</li>
					</ul>
				</div>
	</div>
	</div>
	<div class="single_kanren">
					<h4>関連記事</h4>
					<div class="single_kanren_posts">
						<?php
						// カテゴリーが複数設定されている場合は、どれかをランダムに取得
						$categories = wp_get_post_categories($post->ID, array('orderby'=>'rand'));
						//表示したい記事要素を設定
						if ($categories) {
    					$args = array(
        				'category__in' => array($categories[0]), // カテゴリーのIDで記事を取得
        				'post__not_in' => array($post->ID), // 表示している記事は除外する
        				'showposts'=>6, // 取得したい記事数
        				'caller_get_posts'=>1, // 取得した記事を1番目から表示する
        				'orderby'=> 'rand' // ランダムで取得する
    					); 
						$my_query = new WP_Query($args); 
						if( $my_query->have_posts() ) {
						while ($my_query->have_posts()) { $my_query->the_post(); 
						?>
						<div>
							<a href="<?php the_permalink(); ?>">
    							<?php the_post_thumbnail(); ?>
								<h2><?php the_title(); ?></h2>
							</a>
						</div>
						<?php } wp_reset_query();
						} else { ?>
						<p class="no-related">関連記事はありません</p>
						<?php } } ?>
					</div>
    </div>
    </div>
<?php endwhile; ?>
<?php else: ?>
<div class="notfound"><p>ページがありません</p></div>
<?php endif; ?>
</main>
<?php get_footer(); ?>