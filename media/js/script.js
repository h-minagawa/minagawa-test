// JavaScript Document

//// pageScroll ////
$(document).ready(function () {
    $("#pagetop_btn").hide();
    $(function () {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
                $('#pagetop_btn').fadeIn();
            } else {
                $('#pagetop_btn').fadeOut();
            }
        });

        $('#pagetop_btn a').click(function () {
            $('body,html').animate({
                scrollTop: 0
            }, 600);
            return false;
        });
    });
});