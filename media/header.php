<!DOCTYPE html>
<html>
<head>

<meta charset="<?php bloginfo ('charset') ; ?>" />
<title><?php bloginfo('name'); ?></title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>" type="text/css" />
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/reset.css" type="text/css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/script.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.bxslider.js"></script>
<script>
  $(document).ready(function(){
    $('.bxslider').bxSlider({ //bxsliderを使用しているulクラスを指定
      pager: false,
      slideWidth: 700, //画像の横幅を指定
      minSlides: 1,
      maxSlides: 1,
      moveSlides: 1,
      auto: true
    });
  });
</script>

<?php wp_head(); ?>
</head>

<body>
<div id="wrapper">
<!--  header-->
	<header id="header">
		<div id="header_inner">
			<h1><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/header_logo.png" alt="ヘッダーロゴ"></a></h1>
			<div class="header_content">
			<nav id="gnav">
				<ul class="header_menu">
			    	<li class="toggle">
      				<a href="<?php echo home_url().'/'; ?>magazine">MAGAZINE</a>
      			<div class="menu">
        			<ul class="menu_inner">
          			<li><a href="<?php echo home_url().'/'; ?>magazine/design">デザイン</a></li>
          			<li><a href="<?php echo home_url().'/'; ?>magazine/product">モノ</a></li>
          			<li><a href="<?php echo home_url().'/'; ?>magazine/interior">建築・インテリア</a></li>
          			<li><a href="<?php echo home_url().'/'; ?>magazine/photo">写真</a></li>
          			<li><a href="<?php echo home_url().'/'; ?>magazine/movie">動画</a></li>
          			<li><a href="<?php echo home_url().'/'; ?>magazine/app">アプリ</a></li>
          			<li><a href="<?php echo home_url().'/'; ?>magazine/book">本</a></li>
          			<li><a href="<?php echo home_url().'/'; ?>magazine/life">くらし</a></li>
          			<li><a href="<?php echo home_url().'/'; ?>magazine/trip">旅</a></li>
          			<li><a href="<?php echo home_url().'/'; ?>magazine/shop">お店・場</a></li>
          			<li><a href="<?php echo home_url().'/'; ?>magazine/eat">食</a></li>
          			<li><a href="<?php echo home_url().'/'; ?>magazine/event">イベント・展示</a></li>
          			<li><a href="<?php echo home_url().'/'; ?>magazine/people">インタビュー</a></li>
          			<li><a href="<?php echo home_url().'/'; ?>magazine/local">地域</a></li>
        			</ul>
      			</div>
    				</li>
<!--				    <li><a href="<?php echo home_url().'/'; ?>magazine">MAGAZINE</a></li>-->
				    <li><a href="/about">ABOUT</a></li>
				    <li><a href="<?php echo home_url().'/'; ?>contact">CONTACT</a></li>
			    </ul>
			   </nav>
      <ul class="header_sns">
         <li><a href="https://www.facebook.com/haconiwa.mag"><img src="<?php echo get_template_directory_uri(); ?>/images/icon_facebook.png" alt="facebook"></a></li>
         <li><a href="https://twitter.com/haconiwa_mag"><img src="<?php echo get_template_directory_uri(); ?>/images/icon_twitter.png" alt="twitter"></a></li>
         <li><a href="https://instagram.com/haconiwa_mag"><img src="<?php echo get_template_directory_uri(); ?>/images/icon_instagram.png" alt="instagram"></a></li>
         <li><a href="https://www.pinterest.com/haconiwa_mag/"><img src="<?php echo get_template_directory_uri(); ?>/images/icon_pinterest.png" alt="pinterest"></a></li>
         <li><a id="search" href="javascript:void(0);" style="display: inline;"><img src="<?php echo get_template_directory_uri(); ?>/images/icon_search.png" alt=""></a></li>
      </ul>
			</div>
		</div>
	</header>
<!--  //header  -->