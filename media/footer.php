<?php /* WordPress CMS Theme media */ ?>

<!-- footer -->
	<div id="pagetop_btn">
	   <a href="#wrapper" class="scroll">
	       <img src="<?php echo get_template_directory_uri(); ?>/images/icon_pagetop.png" alt="pagetop">
	   </a>
    </div>
	<footer>
      <div class="footer_inner">
			<p class="copyright"><small>COPYRIGHT (c) media_minagawa. ALL RIGHTS RESERVED.</small></p>
	  	<ul class="footer_menu">
				<li><a href="#">運営会社</a></li>
				<li><a href="#">広告掲載</a></li>
				<li><a href="#">利用規約</a></li>
				<li><a href="#">プライバシーポリシー</a></li>
			</ul>
		</div>
	</footer>
<!-- //footer -->
<?php wp_footer(); ?>
</div>
</body>
</html>