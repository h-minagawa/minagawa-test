<?php /* WordPress CMS Theme media */ get_header(); ?>
<!-----main_visual----->
<div class="main_visual">
<div id="keyimg">
<!-----main_visual_inner----->
    <ul class="bxslider">
    <?php $main_visual = new WP_Query(
        array(
            'posts_per_page' => 5,// 取得する投稿数 「-1」指定で全件表示
            'orderby' => 'date', // 日付でソート
            'order' => 'DESC', // DESCで最新から表示、ASCで最古から表示
            'category_name' => 'uncategorized,app,event,people,shop,life,design,product,photo,movie,local,interior,trip,book,eat' // 表示したいカテゴリーのスラッグを指定
            )
         );
    ?>
    <?php if ( $main_visual->have_posts() ) : ?>
      <?php while ( $main_visual->have_posts() ) : ?>
        <?php $main_visual->the_post(); ?>
        <li>
           <!--パーマリンクの出力-->
           <a href="<?php the_permalink(); ?>">
            <!--画像を追加-->
      			<?php if( has_post_thumbnail() ): ?>
       			<?php the_post_thumbnail(); ?>
      			<?php endif; ?>
      		<!-----slider_text----->
           <div class="slider_text">
            <p>
               <span><?php $str = '';
                foreach((get_the_category()) as $cat){
                    $str .= $cat->cat_name . ', ';
                }
                echo rtrim($str, ", "); ?>
                </span>
                <br><?php the_title(); ?>
            </p>
           </div>
           <!-----//slider_text----->
      		</a>
        </li>
        <?php endwhile; ?>
        <?php endif; ?>
        <?php wp_reset_postdata(); ?>
    </ul>
<!-----//main_visual_inner----->
</div>
</div>
<!-----main_visual----->

<!-----main_content----->
<div id="content">
<p class="catchcopy">女子クリエイターのための<br class="br_sp">ライフスタイルつくりマガジン</p>
<div class="content_inner">
<div id="main_content">
  <div class="magazine">
  	<section id="magazine_inner">
    <?php
        $new_post = new WP_Query(
        array(
            'post_type'      => 'post',// ページタイプを指定
            'posts_per_page' => 10,// 取得する投稿数 「-1」指定で全件表示
            'orderby' => 'date', // 日付でソート
            'order' => 'DESC', // DESCで最新から表示、ASCで最古から表示
            'category_name' => 'uncategorized,app,event,people,shop,life,design,product,photo,movie,local,interior,trip,book,eat' // 表示したいカテゴリーのスラッグを指定
            )
         );
    ?>
    <?php if ( $new_post->have_posts() ) : ?>
      <?php while ( $new_post->have_posts() ) : ?>
        <?php $new_post->the_post(); ?>
        <article>
            <!--パーマリンクの出力-->
          <a href="<?php the_permalink(); ?>">
            <!--画像を追加-->
      			<?php if( has_post_thumbnail() ): ?>
       			<?php the_post_thumbnail(); ?>
      			<?php endif; ?>
            <!--カテゴリー-->
            <p class="magazine_category">
            <?php $str = '';
                foreach((get_the_category()) as $cat){
                    $str .= $cat->cat_name . ', ';
                }
                echo rtrim($str, ", "); ?></p>
            <!--タイトル-->
            <h3><?php the_title(); ?></h3>
            <!--投稿日を表示-->
            <p class="magazine_data"><?php echo get_the_date(); ?></p>
          </a>
        </article>
        <?php endwhile; ?>
        <?php endif; ?>
        <?php wp_reset_postdata(); ?>
    	</section>
        <div id="more_button">
            <a href="<?php echo home_url().'/'; ?>magazine">VIEW ALL</a>
        </div>
    </div>
</div>
<?php get_sidebar(); ?>
</div>
</div>
<!-----//main_content----->
<?php get_footer(); ?>